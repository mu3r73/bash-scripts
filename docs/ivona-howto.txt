how to use the ivona / amazon polly text-to-speech service on the cli:


1. create/login to an aws account with access to the 'polly' (ivona reader) service


2. go to AWS Account > Account Details to get your credentials


3. setup your credentials file:
(WARNING: this needs to be updated periodically - eg: 1 hour for student accounts)

... create/edit file: ~/.aws/credentials
... copy/paste credentials info


4. install aws cli

... for example, on artix/arch linux:
# pacman -S aws-cli


5. configure aws-cli:

$ aws configure

AWS Access Key ID [None]:       # copy/paste from credentials screen
AWS Secret Access Key [None]:   # copy/paste from credentials screen
Default region name [None]:     # from https://docs.aws.amazon.com/general/latest/gr/rande.html#pol_region
Default output format [None]: json   # output format for the console


6. test it:

$ aws polly synthesize-speech --language-code en-US --voice-id Joey --output-format mp3 --text "hello world" hello.mp3
$ mpv hello.mp3   # or whichever player you'd prefer

---

available voices & languages:
https://docs.aws.amazon.com/polly/latest/dg/voicelist.html

full docs:
https://docs.aws.amazon.com/polly/latest/dg/what-is.html
