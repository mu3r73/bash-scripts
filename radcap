#!/bin/bash

# some ansi colors
hi_color="\033[0;36m"
err_color="\033[1;31m"
reset_color="\033[0m"

prog=$(basename "$0")
desc="$prog, a menu-driven selector for radio caprice stations"
syntax="usage:  $prog [OPTIONS]"
where=("options:\n"
       "\t--genre [NUMBER]: choose a genre\n"
       "\t--subgenre [NUMBER]: choose a subgenre (requires: --genre)\n"
       "\t--rnd: play random station\n"
       "\t--update-data: updates $json_file\n"
       "\t--set-black-bg: set black background"
      )
examples=("examples:\n"
          "\tradcap                           asks which genre and subgenre to play\n"
          "\tradcap --rnd                     plays a random station\n"
          "\tradcap --genre 7 --subgenre 46   plays genre 7, subgenre 46\n"
          "\tradcap --genre 1                 asks which subgenre of genre 1 to play\n"
          "\tradcap --genre 6 --rnd           plays a random genre 6 station"
         )
requires="requires: wget, jq, dialog, sed, mpv"

# required .json file will be stored @ $workdir directory (default is $HOME)
workdir=$HOME
json_file="$workdir/radcap.json"
# where to get the .json file if it hasn't been downloaded yet
json_url="https://gist.githubusercontent.com/leejunip/741d5a8330262b73b53eec676e44ec70/raw/61ad5fbc1332a1708c2eed75c417b718e81d4852/radcap.json"

# downloads $json_url to $json_file
# requires: wget
function download_json_file() {
  wget "$json_url" -O "$json_file"
}

# adds background color to ~/.dialogrc (will affect all dialogs)
function set_black_background() {
  dialogrc_file="${HOME}/.dialogrc"
  echo "screen_color = (CYAN,BLACK,ON)" >> $dialogrc_file
  echo "dialog background set to black in $dialogrc_file"
  echo "(this will affect all dialogs, not only radcap's)"
}

# arguments:
#   $1: command line chosen index
#   $2: number of choices
#   $3: choices array
#   $4: choice type
#   $5: dialog title
function get_choice() {
  cl_index=$1
  num_choices=$2
  local -n choices=$3
  choice_type=$4
  dialog_title=$5
  index=-1
  if [ $cl_index ]; then
    # an index was provided via cli
    re="^[0-9]+$"
    if ! [[ $cl_index =~ $re ]]; then
      # not a number
      echo -e "${err_color}error: '$cl_index' is NOT a number${reset_color}"
    elif (( $cl_index >= 0 )) && (( $cl_index <= $num_choices )); then
      # number in valid range
      index=$cl_index
    else
      # number not in valid range
      echo -e "${err_color}error: $choice_type $cl_index doesn't exist${reset_color}"
    fi
  elif [ -z $rnd ]; then
    # no cli index, no --rnd cli option => show choices menu
    index=$(get_choice_from_menu $num_choices choices "$dialog_title" "${choice_type}s:" )
    clear
  else
    index=0
  fi
  return $index
}

# arguments:
#   $1: number of choices
#   $2: choices array
#   $3: dialog title
#   $4: menu title
# returns: chosen_index
function get_choice_from_menu() {
  num_choices=$1
  local -n choices_array=$2
  dialog_title=$3
  menu_title=$4
  
  # generate $options for dialog, $num_choices and $max_width
  max_index=$(( $num_choices - 1 ))
  options=()
  generate_options choices_array $max_index options
  max_width=$(find_max_width choices $max_index 5)

  # show dialog, get result
  # requires: dialog
  height=$(calc_height $num_choices)
  width=$(calc_width $max_width)
  
  chosen_index=$(dialog --stdout --title "$dialog_title" --menu "$menu_title" $height $width $num_choices 0 "(random)" "${options[@]}")
  if (( $? > 0 )); then
    chosen_index=-1
  fi

  echo $chosen_index
}

# arguments:
#   $1: names array
#   $2: max_index of names array
#   $3: options array (output)
# modifies:
#   output array, adding options based on names array
# requires: sed
function generate_options() {
  local -n names=$1
  max_index=$2
  local -n output=$3
  
  for i in $(seq 0 $max_index); do
    name=$(echo ${names[$i]} | sed "s/\&amp;/\&/g")
    output+=("$(( $i + 1 ))" "$name")
  done
}

# arguments:
#   $1: names array
#   $2: max_index of names array
#   $3: padding
# returns: max width of name in names array
function find_max_width() {
  local -n names=$1
  max_index=$2
  padding=$3

  max=0
  for i in $(seq 0 $max_index); do
    name=${names[$i]}
    width=$(( ${#name} + 7 + $padding ))
    if (( $width > $max )); then
      max=$width
    fi
  done
  echo $max
}

# arguments:
#   $1: number of items to show
# returns: height for dialog
function calc_height() {
  num=$1
  screen_height=$(tput lines)
  
  height=$(( $num + 7 ))
  if (( $height > $screen_height )); then
    height=$screen_height
  fi
  echo $height
}

# arguments:
#   $1: max width of items to show
# returns: width for dialog
function calc_width() {
  width=$1
  screen_width=$(tput cols)
  
  if (( $width > $screen_width )); then
    width=$screen_width
  fi
  echo $width
}

# arguments:
#   $1: chosen index
#   $2: range
# returns: base index or random index
function solve_index() {
  choice=$1
  range=$2
  if (( $choice == 0 )); then
    # pick random index
    index=$(( $RANDOM % $range ))
  else
    index=$(( $choice - 1 ))
  fi
  echo $index
}

## main

# parse args 
while true; do
  case "$1" in
    --help)
      echo $desc
      echo "$syntax"
      echo -e ${where[@]}
      echo -e "${examples[@]}"
      echo $requires
      exit
      ;;
    --genre)
      cl_genre_index=$2
      shift
      shift
      ;;
    --subgenre)
      cl_subgenre_index=$2
      shift
      shift
      ;;
    --update-data)
      echo "updating $json_file..."
      download_json_file
      shift
      ;;
    --set-black-bg)
      set_black_background
      shift
      ;;
    --rnd)
      rnd=1
      shift
      ;;
    *)
      if [ "$1" != "" ]; then
        echo -e "${err_color}error: '$1' is not a valid option${reset}"
        exit 1
      fi
      break
      ;;
  esac
done

# see if we need to download the .json file
if [ ! -f "$json_file" ]; then
  echo "$json_file not found - downloading it..."
  download_json_file
fi
json_data=$(cat $json_file)

# populate $genres array
# requires: jq
mapfile -t genres <<< $(jq -r ". | keys[]" <<< "$json_data")
num_genres=${#genres[@]}

if [ $cl_subgenre_index ] && [ -z $cl_genre_index ]; then
  echo -e "${err_color}error: no --genre chosen${reset_color}"
  exit 1
fi

# get genre choice

get_choice "$cl_genre_index" "$num_genres" genres "genre" "$prog"
chosen_index=$?

genre_index=$(solve_index $chosen_index $num_genres)
if (( $genre_index > $num_genres )); then
  exit
fi
genre="${genres[ $genre_index ]}"

# populate $subgenres array
# requires: jq
mapfile -t subgenres <<< $(jq -r ".[\"$genre\"] | values[].name" <<< "$json_data")
num_subgenres=${#subgenres[@]}

# get subgenre choice

get_choice "$cl_subgenre_index" "$num_subgenres" subgenres "subgenre" "$genre"
chosen_index=$?

subgenre_index=$(solve_index $chosen_index $num_subgenres)
if (( $subgenre_index > $num_subgenres )); then
  exit
fi
subgenre="${subgenres[ $subgenre_index ]}"

# show & play chosen stream
# requires: mpv
station_url=$(jq -r ".[\"$genre\"][$subgenre_index].url" <<< "$json_data")
echo -e "${hi_color}radio: $genre > $subgenre${reset_color}"
mpv $station_url
